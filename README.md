# nba-stats

Just an app built to learn Angular.

The app lists all nba franchises and their roster.

Stack:
* Front: Angular 8
* Back: AdonisJS with [node.js client for nba.com API endpoints](https://github.com/bttmly/nba)