import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Team } from '../models/team.model';

@Injectable({
  providedIn: 'root'
})
export class NbaService {
  private baseUrl: string = 'http://localhost:3333/api/nba';
  constructor(private http: HttpClient) {}

  public teamsImage = {
    1610612737: 'Hawks.png',
    1610612738: 'Celtics.png',
    1610612751: 'Nets.png',
    1610612766: 'Hornets.png',
    1610612741: 'Bulls.png',
    1610612739: 'Cavaliers.png',
    1610612742: 'Mavericks.png',
    1610612743: 'Nuggets.png',
    1610612765: 'Pistons.png',
    1610612744: 'Warriors.png',
    1610612745: 'Rockets.gif',
    1610612754: 'Pacers.png',
    1610612746: 'Clippers.png',
    1610612747: 'Lakers.png',
    1610612763: 'Grizzlies.png',
    1610612748: 'Heat.gif',
    1610612749: 'Bucks.png',
    1610612750: 'Timberwolves.png',
    1610612740: 'Pelicans.png',
    1610612752: 'Knicks.gif',
    1610612760: 'Thunder.png',
    1610612753: 'Magic.gif',
    1610612755: '76ers.png',
    1610612756: 'Suns.png',
    1610612757: 'Trail Blazers.png',
    1610612758: 'Kings.png',
    1610612759: 'Spurs.png',
    1610612761: 'Raptors.png',
    1610612762: 'Jazz.png',
    1610612764: 'Wizards.png'
  };

  getUrl(endpoint: string, params?: Array<[string, string]>): string {
    let queryParams: string[] = [];
    let queryString = '';
    if (params) {
      params.forEach(param => queryParams.push(`${param[0]}=${param[1]}`));
      queryString = '?' + queryParams.join('&');
    }

    return `${this.baseUrl}/${endpoint}${queryString}`;
  }

  public getTeams() {
    let url = this.getUrl('teams');
    return this.http.get(url).pipe(map((res: Team[]) => res));
  }

  public getImage(teamId: number) {
    return `assets/logos/${this.teamsImage[teamId]}`;
  }

  public getTeam(teamId: number) {
    let url = this.getUrl(`teams/${teamId}`);
    return this.http.get(url);
  }

  public getPlayer(playerId: number) {
    let url = this.getUrl(`players/${playerId}`);
    return this.http.get(url);
  }

  public findPlayer(query: string) {
    let url = this.getUrl(`find-player?query=${query}`);
    return this.http.get(url);
  }
}
