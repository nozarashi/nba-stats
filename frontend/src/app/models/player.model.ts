export interface Player {
  teamID?: 1610612744;
  season?:  string;
  leagueID?:  string;
  player?:  string;
  num?: string
  position?:  string;
  height?:  string;
  weight?:  string;
  birthDate?:  string;
  age?: number;
  exp?:  string;
  school?:  string;
  playerId?: number;
}
