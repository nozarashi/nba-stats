export interface Team {
  teamId: number;
  teamName: string;
  abbreviation: string;
  simpleName: string;
  location: string;
}
