import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Team } from 'src/app/models/team.model';
import { NbaService } from 'src/app/services/nba.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  teamId;
  teamInfoCommon: any;
  roster: any;
  coach: any;

  constructor(private route: ActivatedRoute, private nba: NbaService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.teamId = params.get('teamId');
      this.nba.getTeam(this.teamId).subscribe(data => {
        this.teamInfoCommon = data['teamInfoCommon'][0];
        this.roster = data['commonTeamRoster']['commonTeamRoster'];
        this.coach = data['commonTeamRoster']['coaches'].find(coach => coach.type = 'Head Coach');
      })
    });
  }

}
