import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbaService } from 'src/app/services/nba.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  hideNav: boolean = true;
  constructor(private router: Router, private nba: NbaService) {}

  ngOnInit() {}

  toggleNavigation() {
    this.hideNav = !this.hideNav;
  }

  findPlayer($event) {
    const query = $event.target.value.trim();

    if (query.length === 0) {
      this.router.navigate(['/']);
      $event.target.value = '';
    } else {
      this.nba.findPlayer(query).subscribe(
        data => this.router.navigate(['/players', data['playerId']]),
        error => {
          if (error.status === 404) {
            alert('Player not found');
          }
        }
      );
    }
  }
}
