import { Component, OnInit } from '@angular/core';
import { NbaService } from 'src/app/services/nba.service';
import { Team } from 'src/app/models/team.model';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {
  teams: Team[];
  constructor(private nba: NbaService) {}

  ngOnInit() {
    this.nba.getTeams().subscribe(data => (this.teams = data));
  }
}
