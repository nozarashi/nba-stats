import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NbaService } from 'src/app/services/nba.service';
import * as differenceInCalendarYears from 'date-fns/difference_in_calendar_years';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
  playerId;
  playerInfos: any;
  headlineStats: any;
  constructor(private route: ActivatedRoute, private nba: NbaService) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.playerId = params.get('playerId');
      this.nba.getPlayer(this.playerId).subscribe(data => {
        this.playerInfos = data['commonPlayerInfo'][0];
        this.headlineStats = data['playerHeadlineStats'][0];
        console.log(this.playerInfos)
      });
    });
  }

  getAge(birthday) {
    return differenceInCalendarYears(new Date(), new Date(birthday));
  }
}
