import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import {HalfCircleSpinnerModule} from 'angular-epic-spinners'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { TeamsComponent } from './components/teams/teams.component';
import { TeamComponent } from './components/team/team.component';
import { PlayerComponent } from './components/player/player.component';

@NgModule({
  declarations: [AppComponent, NavComponent, TeamsComponent, TeamComponent, PlayerComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, HalfCircleSpinnerModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
