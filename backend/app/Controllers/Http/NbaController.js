'use strict';
const NBA = require('nba');

class NbaController {
  teams({ response }) {
    response.send(NBA.teams);
  }

  async team({ params }) {
    const { teamId } = params;
    const year = new Date().getFullYear();

    let teamInfoCommonData = await NBA.stats.teamInfoCommon({
      TeamID: teamId,
      Season: `${year}-${(year % 100) + 1}`
    });

    let { teamInfoCommon } = teamInfoCommonData;
    const commonTeamRoster = await NBA.stats.commonTeamRoster({
      TeamID: teamId,
      Season: `${year}-${(year % 100) + 1}`
    });

    return {
      teamInfoCommon,
      commonTeamRoster
    };
  }

  async player({ params }) {
    const playerInfos = await NBA.stats.playerInfo({
      PlayerID: params.playerId
    });
    return playerInfos;
  }

  async findPlayer({ request, response }) {
    const queryParams = request.get();
    let searchResult = await NBA.findPlayer(queryParams.query);
    let status = 200;
    if (searchResult === undefined) {
      status = 404;
    }
    return response.status(status).send(searchResult);
  }
}

module.exports = NbaController;
